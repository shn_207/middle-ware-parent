package com.notify.event.entity;

import org.springframework.context.ApplicationEvent;

/**
 * @author 公众号:知了一笑
 * @since 2022-04-23 18:19
 */
public class OrderStateEvent extends ApplicationEvent {
    public OrderStateEvent (OrderState orderState){
        super(orderState);
    }
}
